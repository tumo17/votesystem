<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common_csd.css" media="screen and (max-width:601px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main/vote_csd.css" media="screen and (max-width:601px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main/vote.css" media="screen and (min-width:600px)">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Vote</h1>
</header>
<hr>

<main>
	<h2>Select most suitable person for each prize</h2>
	
	<div class="div_redmsg">
		<div id="div_success_vote">${msg_success_vote}</div>
		<div id="div_empty_vote">${errmsg_empty_vote}</div>
		<div id="div_self_vote">${errmsg_self_vote}</div>
		<div id="div_self_vote">${errmsg_duplicate_vote}</div>
		<div id="div_self_vote">${errmsg_vote_system_close}</div>
	</div>
	<div class="div_margin_10px"></div>
	
	<form action="vote.html?@COMMAND=vote" method="post">
		<c:if test="${!empty msg_fin_vote}">
			<p><span id="span_msg_fin_vote">${msg_fin_vote}</span></p>
		</c:if>
		
		<div id="div_vote_wrap">
		<c:forEach items="${point_set_list}" var="pointSet">
			<div class="div_point_wrap">
				<div class="div_point_title">
					${pointSet.point.pointName}
				</div>
				
				<div class="div_point_select">
					<select name="${pointSet.point.pointId}">
						<option value="aa_empty" ord="-1" furigana="aa">-- Please Select --</option>
						<c:forEach items="${pointSet.applicantList}" var="applicant">
							<option value="${applicant.empId}" ord="${applicant.ord}" furigana="${applicant.empKana}" 
										${applicant.empId == pointSet.selectedId ? 'selected' : '' }>
								${applicant.empName}
							</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</c:forEach>
		</div>
		
		<div id="div_sort_condition">
			<label><input type="radio" name="sortType" id="input_radio_ord" checked>Sort by Order</label>
			<label><input type="radio" name="sortType" id="input_radio_alphabet">Sort by Alphabet</label>
			<label><input type="radio" name="sortType" id="input_radio_furigana">Sort by Furigana</label>
		</div>
		
		<input id="input_btn_vote" type="submit" value="Send!!">
	</form>
	<br>
	
	<a href="main.html">Back to main page</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main/vote.js"></script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



