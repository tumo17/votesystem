<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common_csd.css" media="screen and (max-width:601px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main/main.css" media="screen and (min-width:600px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main/main_csd.css" media="screen and (max-width:601px)">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Main</h1>
</header>
<hr>

<main>
	<h2>Hello ${login_user.userName}-san</h2>

	<div id="div_vote_result">
		<c:if test="${empty vote_list}">
			<p>You have no vote result.</p>
		</c:if>
		
		<c:if test="${!empty vote_list}">
			<p>Your vote result</p>
			<table id="tbl_vote_result">
				<tr><th>Prize Name</th><th>Your vote</th></tr>
				<c:forEach items="${vote_list}" var="vote">
					<tr><td>${vote.point.pointName}</td><td>${vote.applicant.empName}</td></tr>
				</c:forEach>
			</table>
		</c:if>
	</div>
	<br>
	
	<a href="vote.html">Go to Vote page</a><br><br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



