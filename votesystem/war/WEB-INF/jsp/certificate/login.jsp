<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common_csd.css" media="screen and (max-width:601px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/certificate/login_csd.css" media="screen and (max-width:601px)">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/certificate/login.css" media="screen and (min-width:600px)">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Login</h1>
</header>
<hr>

<main>
	<form id="form_login" action="login.html?@COMMAND=login" method="POST">
	<div id="div_login">
		<div class="div_box">
			<div class="div_title">User Name</div>
			<div class="div_content"><input type="text" name="userId" value="${val_user}" maxLength="20" placeholder="UserName"></div>
		</div>
		<div class="div_box">
			<div class="div_title">Password</div>
			<div class="div_content"><input type="password" name="password" maxLength="20" placeholder="Password"></div>
		</div>
	</div>
	<div class="div_redmsg">
		<div id="div_empty_user">${errmsg_empty_user}</div>
		<div id="div_empty_pass">${errmsg_empty_pass}</div>
		<div id="div_err_pass">${errmsg_login}</div>
	</div>
	<input id="input_btn_login" type="submit" value="Login">
	</form>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



