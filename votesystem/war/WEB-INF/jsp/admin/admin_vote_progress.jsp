<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/admin_vote_progress.css">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Admin Vote Progress</h1>
</header>
<hr>

<main>
	<p>${vote_progress}</p>
	
	<% int no = 1; %>
	<table id="tbl_vote_progress">
	<tr><th>No</th><th>UserId</th><th>User Name</th><th>Status</th></tr>
	<c:forEach items="${progressList}" var="progressDto">
		<tr><td><%= no %></td><td>${progressDto.user.userId}</td><td>${progressDto.user.userName}</td><td>${progressDto.progress}</td></tr>
		<% no++; %>
	</c:forEach>
	</table><br><br><br>
	
	<a href="admin_home.html">Back to Admin Main page</a><br><br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



