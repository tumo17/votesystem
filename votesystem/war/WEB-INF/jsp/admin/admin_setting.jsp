<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Admin Setting</h1>
</header>
<hr>

<main>
	
	<c:out value="${msg_vote_status}"></c:out><br>
	
	<br>
	<form action="admin_setting.html?@CHANGE_VOTE_STATUS=true" method="post">
		<input type="submit" value="OPEN SYSTEM!!">
	</form><br>
	<form action="admin_setting.html?@CHANGE_VOTE_STATUS=false" method="post">
		<input type="submit" value="CLOSE SYSTEM">
	</form><br><br>
	
	<a href="admin_home.html">Back to Admin Main page</a><br><br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



