<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/admin_vote_result.css">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Admin Vote Result</h1>
</header>
<hr>

<main>
	<h2>Each Prize Results</h2>
	
	<div id="div_prize_wrap">
		<c:forEach items="${prizeSetList}" var="prizeSet">
		<div class="div_prize_result">
			<h3>${prizeSet.prize.prizeName}</h3>
			<table class="tbl_prize_result">
				<tr><th>Name</th><th>Point</th></tr>
				<c:forEach items="${prizeSet.winnerList}" var="winner">
					<tr><td>${winner.empName}</td><td>${winner.point}</td></tr>
				</c:forEach>
			</table>
		</div>
		</c:forEach>
	</div>
		
	<br><br>
	<a href="admin_home.html">Back to Admin Main page</a><br><br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/jquery-1.9.1.js"></script>
<script type="text/javascript">
<!--
$(function() {
	var $divPrizeResult = $('div.div_prize_result');
	var width = $divPrizeResult.width() * $divPrizeResult.length;
	$('div#div_prize_wrap').css({'width':width});
});
-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



