<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/admin_home.css">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Admin Home</h1>
</header>
<hr>

<main>
	<div class="div_admin_menu"><a href="admin_mst_maintenance.html">Mst Maintenance</a></div>
	<div class="div_admin_menu"><a href="admin_vote_result.html">Vote Result</a><br></div>
	<div class="div_admin_menu"><a href="admin_vote_progress.html">Vote Progress</a></div>
	<div class="div_admin_menu"><a href="admin_setting.html">Setting</a><br></div>
	
	<br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



