<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>CWS Result57</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/admin_mst_maintenance.css">
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Admin Create User</h1>
</header>
<hr>

<main>
	
	<div class="div_redmsg">
		<div><c:out value="${msg_create_user_success}"></c:out></div>
	</div>
	
	<br>
	<form action="admin_mst_maintenance.html?@COMMAND=create_user" method="post">
		<table id="tbl_create_user">
			<tr><th>User ID</th><td><input type="text" name="user_id" placeholder="ex:kitagawa_k"></td></tr>
			<tr><th>User Name</th><td><input type="text" name="user_name" placeholder="ex:北川景子"></td></tr>
		</table>
		
		<div class="div_redmsg">
			<div><c:out value="${errmsg_empty_id}"></c:out></div>
			<div><c:out value="${errmsg_empty_name}"></c:out></div>
		</div>
		
		<br>
		<input type="submit" value="CREATE USER">
	</form>
	<br>
	
	<br><br>
	<a href="admin_home.html">Back to Admin Main page</a><br><br>
	<a href="logout.html">Logout</a>
</main>
<hr>

<footer>
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



