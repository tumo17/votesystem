//$(function() {
	var App = {
		init : function() {
			this.bindEvents();
			AppUtil.sortByOrder();
			AppUtil.paintRadio();
		},
		bindEvents : function() {
			$('input#input_radio_ord').on('click', this.radioOrdClick);
			$('input#input_radio_alphabet').on('click', this.radioAlphabetClick);
			$('input#input_radio_furigana').on('click', this.radioFuriganaClick);
		},
		radioOrdClick : function() {
			AppUtil.changeCheck(this);
			AppUtil.sortByOrder();
			AppUtil.paintRadio();
		},
		radioAlphabetClick : function() {
			AppUtil.changeCheck(this);
			AppUtil.sortByAlphabet();
			AppUtil.paintRadio();
		},
		radioFuriganaClick : function() {
			AppUtil.changeCheck(this);
			AppUtil.sortByFurigana();
			AppUtil.paintRadio();
		}
	};
	
	var AppUtil = {
		changeCheck : function($radio) {
			$('input[name=sortType]').removeAttr('checked');
			$($radio).attr('checked','true');
		},
		sortByOrder : function() {
			$('select').each(function() {
				var $optionList = $(this).find('option').sort(function(x,y) {
					return $(x).attr('ord') - $(y).attr('ord');
				});
				$(this).append($optionList);
			});
		},
		sortByAlphabet : function() {
			$('select').each(function() {
				var $optionList = $(this).find('option').sort(function(x,y) {
					return $(x).val() > $(y).val() ? 1 : -1;
				});
				$(this).append($optionList);
			});
		},
		sortByFurigana : function() {
			$('select').each(function() {
				var $optionList = $(this).find('option').sort(function(x,y) {
					return $(x).attr('furigana') > $(y).attr('furigana') ? 1 : -1;
				});
				$(this).append($optionList);
			});
		},
		paintRadio : function($radio) {
			$('label').css({ 'color' : '#000000' });
			$('label').css({ 'background-color' : '#ffffff' });
			$('input[checked]').parent().css({ 'color' : '#ffffff' });
			$('input[checked]').parent().css({ 'background-color' : '#336699' });
		}
	};
	
	App.init();
//});
