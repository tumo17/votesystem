package com.appspot.cwsresult;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.manager.PrizeManager;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.front.zutil.ControlDispatcher;
import com.appspot.cwsresult.system.session.SessionContext;
import com.appspot.cwsresult.system.static_.CwsContext;

@SuppressWarnings("serial")
public class MainServlet extends HttpServlet {

	// Initialize //////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		CwsContext.setServletContext(getServletContext());
		CwsContext.loadCwsControl();
		CwsContext.loadServiceMst();
		EmpManager.loadAllUser(servletContext);
		EmpManager.loadAllApplicant(servletContext);
		PrizeManager.loadAllPrize(servletContext);
		PrizeManager.loadAllPoint(servletContext);
	}

	// Get & Post //////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		SessionContext ctx = new SessionContext(req, res);
		ctx.parseRequestUri();
		ctx.certificated();

		String controllerName = ctx.getControllerName();
		IController controller = ControlDispatcher.getController(controllerName);
		controller.perform(ctx);

		String viewName = controller.getViewName();
		dispacth(req, res, viewName);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

	// Utility /////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void dispacth(HttpServletRequest req, HttpServletResponse res, String viewName) throws ServletException,
			IOException {
		// ViewNameがhtml系ならRedirect
		if (viewName != null && viewName.matches(".html")) {
			res.sendRedirect(viewName);
		}

		// それ以外はjspなのでそのままdispatch
		RequestDispatcher rd = req.getRequestDispatcher(viewName);
		rd.forward(req, res);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
