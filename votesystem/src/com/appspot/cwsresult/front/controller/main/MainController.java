package com.appspot.cwsresult.front.controller.main;

import java.util.ArrayList;
import java.util.List;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.manager.PrizeManager;
import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.model.mst.point.Point;
import com.appspot.cwsresult.back.model.tran.vote.Vote;
import com.appspot.cwsresult.back.model.tran.vote.VoteResult;
import com.appspot.cwsresult.back.model.tran.vote.dao.IVoteDao;
import com.appspot.cwsresult.back.zutil.DaoInjector;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class MainController implements IController {

	public static final String CONTROLLER_NAME = "main";
	public static final String JSP_NAME = "/WEB-INF/jsp/main/main.jsp";

	private IVoteDao voteDao;

	public MainController() {
		voteDao = (IVoteDao)DaoInjector.getTranDaoImpl(IVoteDao.class.getName());
	}

	@Override
	public void perform(SessionContext ctx) {
		List<Vote> voteList = voteDao.findVoteByUserId(ctx.getUser().getUserId());
		if (voteList != null) {
			List<VoteResult> voteResultList = convertVoteResult(voteList);
			ctx.setAttribute(VOTE_LIST, voteResultList);
		}
	}

	// ViewName ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		return JSP_NAME;
	}

	// Utility /////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static List<VoteResult> convertVoteResult(List<Vote> voteList) {
		List<VoteResult> resultList = new ArrayList<>();
		for (Vote vote : voteList) {
			Point point = PrizeManager.getPointMap().get(Integer.valueOf(vote.getPointId()));
			Applicant applicant = EmpManager.getApplicantMap().get(vote.getApplicantId());
			resultList.add(new VoteResult(point, applicant));
		}
		return resultList;
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String VOTE_LIST = "vote_list";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
