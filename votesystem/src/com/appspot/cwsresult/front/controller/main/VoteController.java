package com.appspot.cwsresult.front.controller.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.manager.PrizeManager;
import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.model.mst.point.Point;
import com.appspot.cwsresult.back.model.mst.prize.Prize;
import com.appspot.cwsresult.back.model.tran.vote.Vote;
import com.appspot.cwsresult.back.model.tran.vote.dao.IVoteDao;
import com.appspot.cwsresult.back.model.view.point.PointSetDto;
import com.appspot.cwsresult.back.zutil.DaoInjector;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;
import com.appspot.cwsresult.system.static_.CwsContext;

public class VoteController implements IController {

	public static final String CONTROLLER_NAME = "vote";
	private static final String JSP_NAME = "/WEB-INF/jsp/main/vote.jsp";

	private IVoteDao voteDao;

	public VoteController() {
		voteDao = (IVoteDao)DaoInjector.getTranDaoImpl(IVoteDao.class.getName());
	}

	@Override
	public void perform(SessionContext ctx) {
		makeVoteView(ctx);
		doVote(ctx);
	}

	private void makeVoteView(SessionContext ctx) {
		List<PointSetDto> pointSetList = new ArrayList<>();
		Map<Integer, Point> pointMap = PrizeManager.getPointMap();
		for (Entry<Integer, Point> entry : pointMap.entrySet()) {
			Point point = entry.getValue();
			List<Applicant> applicantList = getTargetApplicantList(point);
			String selectedId = ctx.getParamString(String.valueOf(point.getPointId()));
			pointSetList.add(new PointSetDto(point, applicantList, selectedId));
		}
		ctx.setAttribute(POINT_SET_LIST, pointSetList);

		if (!CwsContext.isVoteSystemOpen()) {
			ctx.setAttribute(ERRMSG_VOTE_SYSTEM_CLOSE, ERRMSG_VOTE_SYSTEM_CLOSE_VAL);
		}
	}

	private void doVote(SessionContext ctx) {
		if (!CONTROLLER_NAME.equals(ctx.getParamString(SessionContext.AT_COMMAND))) {
			return;
		}

		if (!validate(ctx)) {
			return;
		}

		List<Vote> voteList = makeVoteList(ctx);
		voteDao.insertVote(voteList);
		ctx.setAttribute(MSG_SUCCESS_VOTE, MSG_SUCCESS_VOTE_VAL);
	}

	private List<Vote> makeVoteList(SessionContext ctx) {
		List<Vote> voteList = new ArrayList<>();
		Map<Integer, Point> pointMap = PrizeManager.getPointMap();
		for (Entry<Integer, Point> entry : pointMap.entrySet()) {
			int pointId = entry.getKey().intValue();
			String userName = ctx.getUser().getUserId();
			String voteVal = ctx.getParamString(String.valueOf(pointId));
			voteList.add(new Vote(pointId, userName, voteVal));
		}
		return voteList;
	}

	// Validate ////////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean validate(SessionContext ctx) {
		boolean validate = true;
		if (!CwsContext.isVoteSystemOpen()) {
			ctx.setAttribute(ERRMSG_VOTE_SYSTEM_CLOSE, ERRMSG_VOTE_SYSTEM_CLOSE_VAL);
			return false;
		}

		List<Point> pointList = getPointList();
		Set<String> duplicateCheckSet = new HashSet<>();
		for (Point point : pointList) {
			int pointId = point.getPointId();
			String voteValue = ctx.getParamString(String.valueOf(pointId));

			// 白紙票は投票できない
			if (EMPTY.equals(voteValue)) {
				validate = false;
				ctx.setAttribute(ERRMSG_EMPTY_VOTE, ERRMSG_EMPTY_VOTE_VAL);
				continue;
			}

			// 自分に投票はできない
			if (ctx.getUser().getUserId().equals(voteValue)) {
				validate = false;
				ctx.setAttribute(ERRMSG_SELF_VOTE, ERRMSG_SELF_VOTE_VAL);
				continue;
			}

			// 同じ賞に同じ人を投票できない
			String duplicateCheckKey = voteValue + point.getPrizeId();
			if (duplicateCheckSet.contains(duplicateCheckKey)) {
				validate = false;
				ctx.setAttribute(ERRMSG_DUPLICATE_VOTE, ERRMSG_DUPLICATE_VOTE_VAL);
			}
			duplicateCheckSet.add(voteValue + point.getPrizeId());
		}

		return validate;
	}

	// Utility /////////////////////////////////////////////////////////////////////////////////////////////////////////
	private List<Applicant> getTargetApplicantList(Point point) {
		List<Applicant> targetAppList = new ArrayList<>();
		Map<String, Applicant> appMstMap = EmpManager.getApplicantMap();

		Prize prize = PrizeManager.getPrizeMap().get(Integer.valueOf(point.getPrizeId()));
		boolean isRookieFlg = prize.getTargetType() == Prize.TARGET_ROOKIE;

		for (Entry<String, Applicant> entry : appMstMap.entrySet()) {
			Applicant applicant = entry.getValue();
			if (isRookieFlg && applicant.getIsRookie() == Applicant.NON_ROOKIE) {
				continue;
			}
			targetAppList.add(applicant);
		}
		return targetAppList;
	}

	private List<Point> getPointList() {
		List<Point> pointIdList = new ArrayList<>();
		Map<Integer, Point> pointMap = PrizeManager.getPointMap();
		for (Entry<Integer, Point> entry : pointMap.entrySet()) {
			pointIdList.add(entry.getValue());
		}
		return pointIdList;
	}

	// ViewName ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		return JSP_NAME;
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String EMPTY = "aa_empty";
	private static final String POINT_SET_LIST = "point_set_list";

	private static final String ERRMSG_EMPTY_VOTE = "errmsg_empty_vote";
	private static final String ERRMSG_EMPTY_VOTE_VAL = "You can't vote blank value";
	private static final String ERRMSG_SELF_VOTE = "errmsg_self_vote";
	private static final String ERRMSG_SELF_VOTE_VAL = "You can't vote yourself";
	private static final String ERRMSG_DUPLICATE_VOTE = "errmsg_duplicate_vote";
	private static final String ERRMSG_DUPLICATE_VOTE_VAL = "You can't vote same person to one prize";
	private static final String MSG_SUCCESS_VOTE = "msg_success_vote";
	private static final String MSG_SUCCESS_VOTE_VAL = "Your vote is finished";
	private static final String ERRMSG_VOTE_SYSTEM_CLOSE = "errmsg_vote_system_close";
	private static final String ERRMSG_VOTE_SYSTEM_CLOSE_VAL = "This vote system is closed now";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
