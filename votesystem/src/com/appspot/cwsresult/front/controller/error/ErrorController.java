package com.appspot.cwsresult.front.controller.error;

import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class ErrorController implements IController {

	public static final String CONTROLLER_NAME = "error";
	public static final String JSP_NAME = "/WEB-INF/jsp/error/error.jsp";

	@Override
	public void perform(SessionContext ctx) {
	}

	@Override
	public String getViewName() {
		return JSP_NAME;
	}

}
