package com.appspot.cwsresult.front.controller.certificate;

import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class LogoutController implements IController {

	public static final String CONTROLLER_NAME = "logout";
	private static final String JSP_NAME = "/WEB-INF/jsp/certificate/logout.jsp";

	@Override
	public void perform(SessionContext ctx) {
		ctx.getSession().invalidate();
	}

	@Override
	public String getViewName() {
		return JSP_NAME;
	}

}
