package com.appspot.cwsresult.front.controller.certificate;

import java.util.Date;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;
import com.appspot.cwsresult.system.static_.CwsContext;
import com.appspot.cwsresult.system.zutil.StringUtil;

public class LoginController implements IController {

	public static final String CONTROLLER_NAME = "login";
	private static final String JSP_NAME = "/WEB-INF/jsp/certificate/login.jsp";
	private static final String URL_MAIN_CONTROLLER = "main.html";
	private static final String URL_ADMIN_CONTROLLEr = "admin_home.html";

	private CERTIFICATE certificateType;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public LoginController() {
		certificateType = CERTIFICATE.NON;
	}

	// PerformImpl /////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void perform(SessionContext ctx) {
		if (!CONTROLLER_NAME.equals(ctx.getParamString(SessionContext.AT_COMMAND))) {
			return;
		}

		if (!validate(ctx)) {
			return;
		}

		doLogin(ctx);
	}

	// Validation //////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean validate(SessionContext ctx) {
		boolean isValidate = true;

		String username = ctx.getParamString(User.USER_ID);
		if (StringUtil.isEmpty(username)) {
			ctx.setAttribute(ERROR_EMPTY_USER, ERR_EMPTY_USERNAME);
			isValidate = false;
		}

		String password = ctx.getParamString(User.PASSWORD);
		if (StringUtil.isEmpty(password)) {
			ctx.setAttribute(VAL_USER, ctx.getParamString(User.USER_ID));
			ctx.setAttribute(ERROR_EMPTY_PASS, ERR_EMPTY_PASSWORD);
			isValidate = false;
		}

		return isValidate;
	}

	// Certification ///////////////////////////////////////////////////////////////////////////////////////////////////
	private void doLogin(SessionContext ctx) {
		User user = findUser(ctx);
		if (user != null) {
			ctx.getSession().setAttribute(SessionContext.LOGIN_USER, user);
			ctx.setAttribute(SessionContext.LOGIN_USER, user);
		} else {
			ctx.setAttribute(VAL_USER, ctx.getParamString(User.USER_ID));
			ctx.setAttribute(ERROR_LOGIN, ERROR_PASSWORD);
		}
	}

	private User findUser(SessionContext ctx) {
		String userId = ctx.getParamString(User.USER_ID);
		String password = ctx.getParamString(User.PASSWORD);

		// まずadminかどうかの判定
		String admin = CwsContext.getCwsControl().get(CwsContext.ADMIN_PASSWORD);
		if (userId.equals(admin) && password.equals(admin)) {
			certificateType = CERTIFICATE.YES_ADMIN;
			return new User(userId, userId, new Date());
		}

		// 今回はパスワード固定なので、この時点で判定。CWSControlの値と相違があったらreturn null(false);
		if (!password.equals(CwsContext.getCwsControl().get(CwsContext.LOGIN_PASSWORD))) {
			return null;
		}

		// 最初にロードしたAll UserのMapからuserIdをkeyにUserを探す。
		User user = EmpManager.getUserMap().get(userId);
		if (user != null) {
			certificateType = CERTIFICATE.YES_NORMAL;
		}
		return user;
	}

	// View ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		switch (certificateType) {
		case YES_ADMIN:
			return URL_ADMIN_CONTROLLEr;
		case YES_NORMAL:
			return URL_MAIN_CONTROLLER;
		default:
			return JSP_NAME;
		}
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String ERROR_EMPTY_USER = "errmsg_empty_user";
	private static final String ERROR_EMPTY_PASS = "errmsg_empty_pass";
	private static final String ERROR_LOGIN = "errmsg_login";
	private static final String ERR_EMPTY_USERNAME = "Username is empty.";
	private static final String ERR_EMPTY_PASSWORD = "Password is empty.";
	private static final String ERROR_PASSWORD = "Password is wrong.";
	private static final String VAL_USER = "val_user";

	private enum CERTIFICATE {
		YES_ADMIN, YES_NORMAL, NON
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
