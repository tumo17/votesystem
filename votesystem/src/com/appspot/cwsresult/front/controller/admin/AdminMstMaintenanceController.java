package com.appspot.cwsresult.front.controller.admin;

import java.text.MessageFormat;
import java.util.Date;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class AdminMstMaintenanceController implements IController {

	private static final String JSP_NAME = "/WEB-INF/jsp/admin/admin_mst_maintenance.jsp";

	@Override
	public void perform(SessionContext ctx) {
		if (CREATE_USER.equals(ctx.getParamString(SessionContext.AT_COMMAND))) {
			createUser(ctx);
		}
	}

	private void createUser(SessionContext ctx) {
		if (!validate(ctx)) {
			return;
		}

		String userId = ctx.getParamString(USER_ID);
		String userName = ctx.getParamString(USER_NAME);
		User user = new User(userId, userName, new Date());

		EmpManager.getUserMap().put(user.getUserId(), user);
		ctx.setAttribute(MSG_CREATE_USER_SUCCESS, MessageFormat.format(MSG_CREATE_USER_SUCCESS_VAL, user.getUserName()));
	}

	private boolean validate(SessionContext ctx) {
		boolean validate = true;

		String userId = ctx.getParamString(USER_ID);
		if (userId == null || "".equals(userId)) {
			ctx.setAttribute(ERRMSG_EMPTY_ID, ERRMSG_EMPTY_ID_VAL);
			validate = false;
		}

		String userName = ctx.getParamString(USER_NAME);
		if (userName == null || "".equals(userName)) {
			ctx.setAttribute(ERRMSG_EMPTY_NAME, ERRMSG_EMPTY_NAME_VAL);
			validate = false;
		}

		return validate;
	}

	// ViewName ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		return JSP_NAME;
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String CREATE_USER = "create_user";
	private static final String USER_ID = "user_id";
	private static final String USER_NAME = "user_name";

	private static final String ERRMSG_EMPTY_ID = "errmsg_empty_id";
	private static final String ERRMSG_EMPTY_ID_VAL = "User ID is required.";
	private static final String ERRMSG_EMPTY_NAME = "errmsg_empty_name";
	private static final String ERRMSG_EMPTY_NAME_VAL = "User Name is required.";
	private static final String MSG_CREATE_USER_SUCCESS = "msg_create_user_success";
	private static final String MSG_CREATE_USER_SUCCESS_VAL = "User \"{0}\" is created !!";
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
