package com.appspot.cwsresult.front.controller.admin;

import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;
import com.appspot.cwsresult.system.static_.CwsContext;

public class AdminSettingController implements IController {

	private static final String JSP_NAME = "/WEB-INF/jsp/admin/admin_setting.jsp";

	@Override
	public void perform(SessionContext ctx) {
		if (ctx.getParamString(AT_CHANGE_VOTE_STATUS) != null) {
			boolean inputSystemOpen = new Boolean(ctx.getParamString(AT_CHANGE_VOTE_STATUS));
			CwsContext.setVoteSystemOpen(inputSystemOpen);
			ctx.setAttribute(MSG_VOTE_STATUS, inputSystemOpen ? "Vote Open !!" : "Vote Close !!");
		}
	}

	@Override
	public String getViewName() {
		return JSP_NAME;
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String AT_CHANGE_VOTE_STATUS = "@CHANGE_VOTE_STATUS";
	private static final String MSG_VOTE_STATUS = "msg_vote_status";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
