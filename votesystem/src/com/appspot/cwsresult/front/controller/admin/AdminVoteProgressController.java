package com.appspot.cwsresult.front.controller.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.back.model.tran.vote.Vote;
import com.appspot.cwsresult.back.model.tran.vote.dao.IVoteDao;
import com.appspot.cwsresult.back.model.view.progress.ProgressDto;
import com.appspot.cwsresult.back.zutil.DaoInjector;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class AdminVoteProgressController implements IController {

	public static final String CONTROLLER_NAME = "admin_vote_progress";
	private static final String JSP_NAME = "/WEB-INF/jsp/admin/admin_vote_progress.jsp";
	private IVoteDao voteDao = (IVoteDao)DaoInjector.getTranDaoImpl(IVoteDao.class.getName());

	@Override
	public void perform(SessionContext ctx) {
		List<ProgressDto> progressList = new ArrayList<>();

		Map<String, User> allUserMap = EmpManager.getUserMap();
		List<Vote> voteList = voteDao.findAllVote();
		int allCnt = allUserMap.size();
		int cnt = 0;

		for (Entry<String, User> entry : allUserMap.entrySet()) {
			boolean isVoted = false;
			for (Vote vote : voteList) {
				if (entry.getKey().equals(vote.getUserId())) {
					isVoted = true;
				}
			}
			if (isVoted) {
				cnt++;
			}
			ProgressDto progressDto = new ProgressDto(entry.getValue(), isVoted ? "○" : "×");
			progressList.add(progressDto);
		}

		ctx.setAttribute("vote_progress", cnt + " / " + allCnt);
		ctx.setAttribute("progressList", progressList);
	}

	// ViewName ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		return JSP_NAME;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
