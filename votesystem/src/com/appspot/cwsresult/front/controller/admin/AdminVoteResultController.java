package com.appspot.cwsresult.front.controller.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.appspot.cwsresult.back.manager.EmpManager;
import com.appspot.cwsresult.back.manager.PrizeManager;
import com.appspot.cwsresult.back.model.mst.point.Point;
import com.appspot.cwsresult.back.model.mst.prize.Prize;
import com.appspot.cwsresult.back.model.tran.vote.Vote;
import com.appspot.cwsresult.back.model.tran.vote.dao.IVoteDao;
import com.appspot.cwsresult.back.model.view.prize.PrizeSetDto;
import com.appspot.cwsresult.back.model.view.prize.WinnerDto;
import com.appspot.cwsresult.back.zutil.DaoInjector;
import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.system.session.SessionContext;

public class AdminVoteResultController implements IController {

	public static final String CONTROLLER_NAME = "admin_vote_result";
	private static final String JSP_VOTE_RESULT = "/WEB-INF/jsp/admin/admin_vote_result.jsp";
	private IVoteDao voteDao = (IVoteDao)DaoInjector.getTranDaoImpl(IVoteDao.class.getName());

	@Override
	public void perform(SessionContext ctx) {
		List<PrizeSetDto> prizeSetList = new ArrayList<>();
		List<Vote> voteList = voteDao.findAllVote();

		Map<Integer, Prize> prizeMap = PrizeManager.getPrizeMap();
		for (Entry<Integer, Prize> entry : prizeMap.entrySet()) {
			Prize prize = entry.getValue();
			List<WinnerDto> winnerList = sumupEachPrize(prize, voteList);
			PrizeSetDto prizeSet = new PrizeSetDto(prize, winnerList);
			prizeSetList.add(prizeSet);
		}

		ctx.setAttribute("prizeSetList", prizeSetList);
	}

	// Vote Result /////////////////////////////////////////////////////////////////////////////////////////////////////
	private List<WinnerDto> sumupEachPrize(Prize prize, List<Vote> voteList) {
		Map<String, Integer> winnerMap = new HashMap<>();
		for (Vote vote : voteList) {
			int pointId = vote.getPointId();
			Point point = PrizeManager.getPointMap().get(Integer.valueOf(pointId));
			if (prize.getPrizeId() != point.getPrizeId()) {
				continue;
			}

			String winnerName = vote.getApplicantId();
			Integer totalPoint = winnerMap.get(winnerName);
			int totalPointInt = totalPoint == null ? 0 : totalPoint.intValue();

			int thisPoint = point.getPoint();
			totalPointInt += thisPoint;
			winnerMap.put(winnerName, Integer.valueOf(totalPointInt));
		}

		List<WinnerDto> winnerList = new ArrayList<>();
		for (Entry<String, Integer> entry : winnerMap.entrySet()) {
			String winnerId = entry.getKey();
			String winnerName = EmpManager.getApplicantMap().get(winnerId).getEmpName();
			int point = entry.getValue().intValue();
			winnerList.add(new WinnerDto(winnerId, winnerName, point));
		}
		Collections.sort(winnerList);
		return winnerList;
	}

	// ViewName ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getViewName() {
		return JSP_VOTE_RESULT;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
