package com.appspot.cwsresult.front.controller;

import com.appspot.cwsresult.system.session.SessionContext;

public interface IController {
	public void perform(SessionContext ctx);
	public String getViewName();
}
