package com.appspot.cwsresult.front.zutil;

import com.appspot.cwsresult.front.controller.IController;
import com.appspot.cwsresult.front.controller.error.ErrorController;
import com.appspot.cwsresult.system.static_.CwsContext;

public class ControlDispatcher {

	private ControlDispatcher() {
	}

	public static IController getController(String controllerName) {
		IController controller = null;
		String controllerClass = CwsContext.getServiceMst().get(controllerName);
		try {
			Class<?> clazz = Class.forName(controllerClass);
			controller = (IController)clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			controller = new ErrorController();
		}
		return controller;
	}

}
