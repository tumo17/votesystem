package com.appspot.cwsresult.back.zutil;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public class PMF {

	private static final String T_OPT = "transactions-optional";
	private static final PersistenceManagerFactory pmfInstance = JDOHelper.getPersistenceManagerFactory(T_OPT);

	private PMF() {
	}

	public static PersistenceManagerFactory get() {
		return pmfInstance;
	}

}
