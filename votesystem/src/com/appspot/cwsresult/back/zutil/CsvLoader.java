package com.appspot.cwsresult.back.zutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy;

public class CsvLoader {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map loadCsv(ServletContext servletContext, String path, Class clazz, String methodName) {
		Map resultMap = Collections.synchronizedMap(new LinkedHashMap<>());
		try {
			URL url = servletContext.getResource(path);
			InputStream inputStream = new FileInputStream(new File(url.getPath().replaceAll("%20", " ")));
			CSVReader reader = new CSVReader(new InputStreamReader(inputStream, "UTF-8"));

			HeaderColumnNameMappingStrategy<Object> mappingStrategy = new HeaderColumnNameMappingStrategy<Object>();
			mappingStrategy.setType(clazz);

			CsvToBean<Object> csvToBean = new CsvToBean<Object>();
			List<Object> objList = csvToBean.parse(mappingStrategy, reader);

			Method method = clazz.getMethod(methodName);
			for (Object obj : objList) {
				Object key = method.invoke(obj);
				resultMap.put(key, obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultMap;
	}

}
