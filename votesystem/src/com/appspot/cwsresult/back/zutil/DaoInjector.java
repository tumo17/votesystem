package com.appspot.cwsresult.back.zutil;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.system.static_.CwsContext;

public class DaoInjector {

	private static final String DAO_MOC = "Moc";
	private static final String MST_DATA_DAO_TYPE = "mst_dao_type";
	private static final String TRAN_DATA_DAO_TYPE = "tran_dao_type";
	//	private static final String DAO_JDO = "jdo";

	private DaoInjector() {
	}

	// GetDaoImpl //////////////////////////////////////////////////////////////////////////////////////////////////////
	public static IDao getMstDaoImpl(String interfaceName) {
		String daoName = getDaoName(interfaceName, MST_DATA_DAO_TYPE);
		return getDaoInstance(daoName);
	}

	public static IDao getTranDaoImpl(String interfaceName) {
		String daoName = getDaoName(interfaceName, TRAN_DATA_DAO_TYPE);
		return getDaoInstance(daoName);
	}

	// Utility /////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static String getDaoName(String interfaceName, String propertyName) {
		int lastDot = interfaceName.lastIndexOf('.');
		int length = interfaceName.length();

		StringBuilder daoName = new StringBuilder();
		daoName.append(interfaceName.substring(0, lastDot + 1));
		daoName.append(interfaceName.substring(lastDot + 2, length));
		daoName.append(getDaoType(propertyName));
		return daoName.toString();
	}

	private static String getDaoType(String propertyName) {
		String daoType = CwsContext.getCwsControl().get(propertyName);
		return daoType != null ? daoType : DAO_MOC;
	}

	private static IDao getDaoInstance(String daoName) {
		IDao dao = null;
		try {
			Class<?> clazz = Class.forName(daoName);
			dao = (IDao)clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dao;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
