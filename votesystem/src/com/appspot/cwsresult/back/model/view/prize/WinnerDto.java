package com.appspot.cwsresult.back.model.view.prize;

public class WinnerDto implements Comparable<WinnerDto> {

	private String empId;
	private String empName;
	private int point;

	public WinnerDto(String empId, String empName, int point) {
		this.empId = empId;
		this.empName = empName;
		this.point = point;
	}

	public String getEmpId() {
		return empId;
	}

	public String getEmpName() {
		return empName;
	}

	public int getPoint() {
		return point;
	}

	@Override
	public int compareTo(WinnerDto w) {
		return w.getPoint() - point;
	}

}
