package com.appspot.cwsresult.back.model.view.point;

import java.util.List;

import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.model.mst.point.Point;

public class PointSetDto {

	private Point point;
	private List<Applicant> applicantList;
	private String selectedId;

	public PointSetDto(Point point, List<Applicant> applicantList, String selectedId) {
		this.point = point;
		this.applicantList = applicantList;
		this.selectedId = selectedId;
	}

	// Getter & Setter /////////////////////////////////////////////////////////////////////////////////////////////////
	public Point getPoint() {
		return point;
	}

	public List<Applicant> getApplicantList() {
		return applicantList;
	}

	public String getSelectedId() {
		return selectedId;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
