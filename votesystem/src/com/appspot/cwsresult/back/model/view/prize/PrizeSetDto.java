package com.appspot.cwsresult.back.model.view.prize;

import java.util.List;

import com.appspot.cwsresult.back.model.mst.prize.Prize;

public class PrizeSetDto {

	private Prize prize;
	private List<WinnerDto> winnerList;

	public PrizeSetDto(Prize prize, List<WinnerDto> winnerList) {
		this.prize = prize;
		this.winnerList = winnerList;
	}

	public Prize getPrize() {
		return prize;
	}

	public List<WinnerDto> getWinnerList() {
		return winnerList;
	}

}
