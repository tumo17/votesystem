package com.appspot.cwsresult.back.model.view.progress;

import com.appspot.cwsresult.back.model.mst.user.User;

public class ProgressDto {

	User user;
	String progress;

	public ProgressDto(User user, String progress) {
		this.user = user;
		this.progress = progress;
	}

	public User getUser() {
		return user;
	}

	public String getProgress() {
		return progress;
	}

}
