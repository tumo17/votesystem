package com.appspot.cwsresult.back.model.mst.point.dao;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.back.model.mst.point.Point;

public interface IPointDao extends IDao {
	public Map<Integer, Point> findAllPoint(ServletContext servletContext);
}
