package com.appspot.cwsresult.back.model.mst.prize.dao;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.prize.Prize;

public class PrizeDaoMoc implements IPrizeDao {

	Map<Integer, Prize> prizeMap = Collections.synchronizedMap(new HashMap<Integer, Prize>());

	@Override
	public Map<Integer, Prize> findAllPrize(ServletContext servletContext) {
		prizeMap.put(Integer.valueOf(0), new Prize(0, "沢村賞", Prize.TARGET_NORMAL, new Date()));
		prizeMap.put(Integer.valueOf(1), new Prize(1, "奪三振賞", Prize.TARGET_NORMAL, new Date()));
		prizeMap.put(Integer.valueOf(2), new Prize(2, "新人賞", Prize.TARGET_ROOKIE, new Date()));

		for (Entry<Integer, Prize> entry : prizeMap.entrySet()) {
			System.out.println(entry.getValue());
		}
		return prizeMap;
	}

}
