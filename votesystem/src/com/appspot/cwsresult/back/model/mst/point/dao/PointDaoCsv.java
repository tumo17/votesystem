package com.appspot.cwsresult.back.model.mst.point.dao;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.point.Point;
import com.appspot.cwsresult.back.zutil.CsvLoader;

public class PointDaoCsv implements IPointDao {

	private static final String TARGET_URL = "/WEB-INF/data/point.csv";

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Point> findAllPoint(ServletContext servletContext) {
		Map<Integer, Point> pointMap = CsvLoader.loadCsv(servletContext, TARGET_URL, Point.class, "getKey");
		printPoint(pointMap);
		return pointMap;
	}

	private void printPoint(Map<Integer, Point> prizeMap) {
		for (Entry<Integer, Point> entry : prizeMap.entrySet()) {
			System.out.println(entry.getValue());
		}
	}

}
