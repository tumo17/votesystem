package com.appspot.cwsresult.back.model.mst.point;

import java.util.Date;

public class Point {

	private int pointId;
	private String pointName;
	private int prizeId;
	private int point;
	private Date prcDate;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public Point() {
	}

	public Point(int pointId, String pointName, int prizeId, int point, Date prcDate) {
		this.pointId = pointId;
		this.pointName = pointName;
		this.prizeId = prizeId;
		this.point = point;
		this.prcDate = prcDate;
	}

	// Getter //////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getPointId() {
		return pointId;
	}
	/** This method is used only loading from csv */
	public Integer getKey() {
		return Integer.valueOf(pointId);
	}
	public String getPointName() {
		return pointName;
	}
	public int getPrizeId() {
		return prizeId;
	}
	public int getPoint() {
		return point;
	}
	public Date getPrcDate() {
		return prcDate;
	}
	public void setPointId(int pointId) {
		this.pointId = pointId;
	}
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	public void setPrizeId(int prizeId) {
		this.prizeId = prizeId;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public void setPrcDate(Date prcDate) {
		this.prcDate = prcDate;
	}

	// Util ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return pointId + " : " + "PointName = " + pointName + ", Prize = " + prizeId + ", Point = " + point;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
