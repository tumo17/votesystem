package com.appspot.cwsresult.back.model.mst.user.dao;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.back.zutil.CsvLoader;

public class UserDaoCsv implements IUserDao {

	private static final String TARGET_URL = "/WEB-INF/data/user.csv";

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, User> findAllUser(ServletContext servletContext) {
		Map<String, User> userMap = CsvLoader.loadCsv(servletContext, TARGET_URL, User.class, "getUserId");
		printAllUser(userMap);
		return userMap;
	}

	@Override
	public void createUser(String userId, String name) {
	}

	private void printAllUser(Map<String, User> userMap) {
		for (Entry<String, User> entry : userMap.entrySet()) {
			System.out.println(entry.getValue());
		}
	}

}
