package com.appspot.cwsresult.back.model.mst.user.dao;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.user.User;

public class UserDaoMoc implements IUserDao {

	Map<String, User> userMap = Collections.synchronizedMap(new TreeMap<String, User>());

	public UserDaoMoc() {
		userMap.put("shibata_ts", new User("shibata_ts", "芝田積", new Date()));
		userMap.put("matsuda_d", new User("matsuda_d", "松田大輝", new Date()));
		userMap.put("kohanawa_k", new User("kohanawa_k", "小塙健太", new Date()));
	}

	@Override
	public Map<String, User> findAllUser(ServletContext servletContext) {
		for (Entry<String, User> entry : userMap.entrySet()) {
			System.out.println(entry.getValue());
		}
		return userMap;
	}

	@Override
	public void createUser(String userId, String name) {
		System.out.println("Create new user");
	}

}
