package com.appspot.cwsresult.back.model.mst.applicant.dao;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.applicant.Applicant;

public class ApplicantDaoMoc implements IApplicantDao {

	Map<String, Applicant> applicantMap = Collections.synchronizedMap(new TreeMap<String, Applicant>());

	@Override
	public Map<String, Applicant> findAllApplicant(ServletContext servletContext) {
		Applicant app01 = new Applicant("shibata_ts", "芝田積", "しばた", "Tokyo", Applicant.NON_ROOKIE, 1, new Date());
		Applicant app02 = new Applicant("matsuda_d", "松田大輝", "まつだ", "Tokyo", Applicant.YES_ROOKIE, 2, new Date());
		Applicant app03 = new Applicant("kohanawa_k", "小塙健太", "こはなわ", "Tokyo", Applicant.YES_ROOKIE, 3, new Date());
		Applicant app04 = new Applicant("darvish_yu", "ダルビッシュ有", "ダル", "Tokyo", Applicant.NON_ROOKIE, 4, new Date());
		Applicant app05 = new Applicant("uehara_k", "上原浩治", "うえはら", "Tokyo", Applicant.NON_ROOKIE, 5, new Date());
		Applicant app06 = new Applicant("kuroda_h", "黒田博樹", "くろだ", "Tokyo", Applicant.NON_ROOKIE, 6, new Date());
		applicantMap.put(app01.getEmpId(), app01);
		applicantMap.put(app02.getEmpId(), app02);
		applicantMap.put(app03.getEmpId(), app03);
		applicantMap.put(app04.getEmpId(), app04);
		applicantMap.put(app05.getEmpId(), app05);
		applicantMap.put(app06.getEmpId(), app06);

		for (Entry<String, Applicant> entry : applicantMap.entrySet()) {
			System.out.println(entry.getValue());
		}
		return applicantMap;
	}

	@Override
	public void createApplicant(String empId, String empName, String location, int isRookie, int ord) {
		System.out.println("Create new applicant");
	}

}
