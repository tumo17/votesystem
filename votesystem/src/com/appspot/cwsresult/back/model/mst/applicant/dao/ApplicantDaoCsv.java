package com.appspot.cwsresult.back.model.mst.applicant.dao;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.zutil.CsvLoader;

public class ApplicantDaoCsv implements IApplicantDao {

	private static final String TARGET_URL = "/WEB-INF/data/applicant.csv";

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Applicant> findAllApplicant(ServletContext servletContext) {
		Map<String, Applicant> applicantMap = CsvLoader
				.loadCsv(servletContext, TARGET_URL, Applicant.class, "getEmpId");
		printAllApplicant(applicantMap);
		return applicantMap;
	}

	@Override
	public void createApplicant(String empId, String empName, String location, int isRookie, int ord) {
	}

	private void printAllApplicant(Map<String, Applicant> applicantMap) {
		for (Entry<String, Applicant> entry : applicantMap.entrySet()) {
			System.out.println(entry.getValue());
		}
	}

}
