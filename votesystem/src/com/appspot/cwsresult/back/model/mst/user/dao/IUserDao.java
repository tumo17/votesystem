package com.appspot.cwsresult.back.model.mst.user.dao;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.back.model.mst.user.User;

public interface IUserDao extends IDao {
	public Map<String, User> findAllUser(ServletContext servletContext);
	public void createUser(String userId, String name);
}
