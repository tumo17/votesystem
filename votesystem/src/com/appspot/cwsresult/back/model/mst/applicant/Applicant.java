package com.appspot.cwsresult.back.model.mst.applicant;

import java.util.Date;

public class Applicant {
	public static final int NON_ROOKIE = 0;
	public static final int YES_ROOKIE = 1;

	private String empId;
	private String empName;
	private String empKana;
	private String location;
	private int isRookie;
	private int ord;
	private Date prcDate;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public Applicant() {
	}

	public Applicant(String empId, String empName, String empKana, String location, int isRookie, int ord, Date prcDate) {
		this.empId = empId;
		this.empName = empName;
		this.empKana = empKana;
		this.location = location;
		this.isRookie = isRookie;
		this.ord = ord;
		this.prcDate = prcDate;
	}

	// Getter //////////////////////////////////////////////////////////////////////////////////////////////////////////
	public String getEmpId() {
		return empId;
	}
	public String getEmpName() {
		return empName;
	}
	public String getEmpKana() {
		return empKana;
	}
	public String getLocation() {
		return location;
	}
	public int getIsRookie() {
		return isRookie;
	}
	public int getOrd() {
		return ord;
	}
	public Date getPrcDate() {
		return prcDate;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public void setEmpKana(String empKana) {
		this.empKana = empKana;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public void setIsRookie(int isRookie) {
		this.isRookie = isRookie;
	}
	public void setOrd(int ord) {
		this.ord = ord;
	}
	public void setPrcDate(Date prcDate) {
		this.prcDate = prcDate;
	}

	// Util ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return ord + " : " + empId + ", " + empName + ", " + location + ", " + getIsRookieString(isRookie);
	}

	private String getIsRookieString(int isRookie) {
		return isRookie == YES_ROOKIE ? "Rookie" : "Member";
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
