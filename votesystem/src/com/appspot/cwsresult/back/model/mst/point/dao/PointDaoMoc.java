package com.appspot.cwsresult.back.model.mst.point.dao;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.point.Point;

public class PointDaoMoc implements IPointDao {

	Map<Integer, Point> pointMap = Collections.synchronizedMap(new HashMap<Integer, Point>());

	@Override
	public Map<Integer, Point> findAllPoint(ServletContext servletContext) {
		pointMap.put(Integer.valueOf(0), new Point(0, "沢村賞(3pt)", 0, 3, new Date()));
		pointMap.put(Integer.valueOf(1), new Point(1, "沢村賞(2pt)", 0, 2, new Date()));
		pointMap.put(Integer.valueOf(2), new Point(2, "沢村賞(1pt)", 0, 1, new Date()));
		pointMap.put(Integer.valueOf(3), new Point(3, "奪三振賞", 1, 1, new Date()));
		pointMap.put(Integer.valueOf(4), new Point(4, "新人賞", 2, 1, new Date()));

		for (Entry<Integer, Point> entry : pointMap.entrySet()) {
			System.out.println(entry.getValue());
		}
		return pointMap;
	}

}
