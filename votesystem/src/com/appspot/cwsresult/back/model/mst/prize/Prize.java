package com.appspot.cwsresult.back.model.mst.prize;

import java.util.Date;

public class Prize {

	private int prizeId;
	private String prizeName;
	private int targetType;
	private Date prcDate;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public Prize() {
	}

	public Prize(int prizeId, String prizeName, int targetType, Date prcDate) {
		this.prizeId = prizeId;
		this.prizeName = prizeName;
		this.targetType = targetType;
		this.prcDate = prcDate;
	}

	// Getter //////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getPrizeId() {
		return prizeId;
	}
	public String getPrizeName() {
		return prizeName;
	}
	public int getTargetType() {
		return targetType;
	}
	public Date getPrcDate() {
		return prcDate;
	}
	public void setPrizeId(int prizeId) {
		this.prizeId = prizeId;
	}
	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}
	public void setTargetType(int targetType) {
		this.targetType = targetType;
	}
	public void setPrcDate(Date prcDate) {
		this.prcDate = prcDate;
	}

	// Util ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return prizeId + " : " + "Name = " + prizeName + ", TargetType = " + getTargetType(targetType);
	}

	public String getTargetType(int targetType) {
		return targetType == TARGET_ROOKIE ? "Rookie" : "Normal";
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static final int TARGET_NORMAL = 1;
	public static final int TARGET_ROOKIE = 2;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
