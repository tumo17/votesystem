package com.appspot.cwsresult.back.model.mst.prize.dao;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.prize.Prize;
import com.appspot.cwsresult.back.zutil.CsvLoader;

public class PrizeDaoCsv implements IPrizeDao {

	private static final String TARGET_URL = "/WEB-INF/data/prize.csv";

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Prize> findAllPrize(ServletContext servletContext) {
		Map<Integer, Prize> prizeMap = CsvLoader.loadCsv(servletContext, TARGET_URL, Prize.class, "getPrizeId");
		printPrize(prizeMap);
		return prizeMap;
	}

	private void printPrize(Map<Integer, Prize> prizeMap) {
		for (Entry<Integer, Prize> entry : prizeMap.entrySet()) {
			System.out.println(entry.getValue());
		}
	}

}
