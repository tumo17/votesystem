package com.appspot.cwsresult.back.model.mst.prize.dao;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.back.model.mst.prize.Prize;

public interface IPrizeDao extends IDao {
	public Map<Integer, Prize> findAllPrize(ServletContext servletContext);
}
