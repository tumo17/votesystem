package com.appspot.cwsresult.back.model.mst.user;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class User implements Serializable {

	private String userId;
	private String userName;
	private Date prcDate;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public User() {
	}

	public User(String userId, String userName, Date prcDate) {
		this.userId = userId;
		this.userName = userName;
		this.prcDate = prcDate;
	}

	// Getter //////////////////////////////////////////////////////////////////////////////////////////////////////////
	public String getUserId() {
		return userId;
	}
	public String getUserName() {
		return userName;
	}
	public Date getPrcDate() {
		return prcDate;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPrcDate(Date prcDate) {
		this.prcDate = prcDate;
	}

	// Util ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return "ID : " + userId + ", Name : " + userName;
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static final String USER_ID = "userId";
	public static final String PASSWORD = "password";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
