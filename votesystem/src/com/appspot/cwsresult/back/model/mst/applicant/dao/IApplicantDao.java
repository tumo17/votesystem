package com.appspot.cwsresult.back.model.mst.applicant.dao;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.back.model.mst.applicant.Applicant;

public interface IApplicantDao extends IDao {
	public Map<String, Applicant> findAllApplicant(ServletContext servletContext);
	public void createApplicant(String empId, String empName, String location, int isRookie, int ord);
}
