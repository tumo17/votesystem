package com.appspot.cwsresult.back.model.tran.vote.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import com.appspot.cwsresult.back.model.tran.vote.Vote;

public class VoteDaoMoc implements IVoteDao {

	private Map<String, List<Vote>> voteListMap = new HashMap<>();

	public VoteDaoMoc() {
		List<Vote> voteList = new ArrayList<Vote>();
		voteList.add(new Vote(0, "shibata_ts", "darvish_yu"));
		voteList.add(new Vote(1, "shibata_ts", "uehara_k"));
		voteList.add(new Vote(2, "shibata_ts", "kuroda_h"));
		voteList.add(new Vote(3, "shibata_ts", "darvish_yu"));
		voteList.add(new Vote(4, "shibata_ts", "matsuda_d"));
		voteListMap.put("shibata_ts", voteList);
	}

	@Override
	public List<Vote> findAllVote() {
		List<Vote> allVoteList = new ArrayList<>();
		for (Entry<String, List<Vote>> entry : voteListMap.entrySet()) {
			allVoteList.addAll(entry.getValue());
		}
		return allVoteList;
	}

	@Override
	public List<Vote> findVoteByUserId(String userId) {
		if (new Random().nextBoolean()) {
			return null;
		}
		return voteListMap.get(userId);
	}

	@Override
	public void insertVote(List<Vote> voteList) {
		System.out.println("Insert your vote");
	}

}
