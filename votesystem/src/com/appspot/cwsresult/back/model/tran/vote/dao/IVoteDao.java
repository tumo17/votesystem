package com.appspot.cwsresult.back.model.tran.vote.dao;

import java.util.List;

import com.appspot.cwsresult.back.model.IDao;
import com.appspot.cwsresult.back.model.tran.vote.Vote;

public interface IVoteDao extends IDao {
	public List<Vote> findAllVote();
	public List<Vote> findVoteByUserId(String userId);
	public void insertVote(List<Vote> voteList);
}
