package com.appspot.cwsresult.back.model.tran.vote.dao;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.appspot.cwsresult.back.model.tran.vote.Vote;
import com.appspot.cwsresult.back.zutil.PMF;

public class VoteDaoJdo implements IVoteDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Vote> findAllVote() {
		List<Vote> voteList = new ArrayList<>();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query query = pm.newQuery(Vote.class);
			voteList = (List<Vote>)query.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
		return voteList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Vote> findVoteByUserId(String userId) {
		List<Vote> voteList = new ArrayList<>();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query query = pm.newQuery(Vote.class);
			query.setFilter("userId == paramId");
			query.declareParameters("String paramId");
			voteList = (List<Vote>)query.execute(userId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
		return voteList;
	}

	@Override
	public void insertVote(List<Vote> voteList) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistentAll(voteList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
		System.out.println("Insert Vote");
	}

}
