package com.appspot.cwsresult.back.model.tran.vote;

import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.model.mst.point.Point;

public class VoteResult {

	private Point point;
	private Applicant applicant;

	public VoteResult(Point point, Applicant applicant) {
		this.point = point;
		this.applicant = applicant;
	}

	public Point getPoint() {
		return point;
	}

	public Applicant getApplicant() {
		return applicant;
	}

}
