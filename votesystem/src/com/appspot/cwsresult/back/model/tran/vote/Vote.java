package com.appspot.cwsresult.back.model.tran.vote;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class Vote {

	@PrimaryKey private String voteId;
	@Persistent private int pointId;
	@Persistent private String userId;
	@Persistent private String applicantId;

	public Vote(int pointId, String userId, String applicantId) {
		this.voteId = pointId + userId;
		this.pointId = pointId;
		this.userId = userId;
		this.applicantId = applicantId;
	}

	// Getter & Setter /////////////////////////////////////////////////////////////////////////////////////////////////
	public String getVoteId() {
		return voteId;
	}
	public int getPointId() {
		return pointId;
	}
	public String getUserId() {
		return userId;
	}
	public String getApplicantId() {
		return applicantId;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
