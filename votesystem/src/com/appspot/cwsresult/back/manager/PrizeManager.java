package com.appspot.cwsresult.back.manager;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.point.Point;
import com.appspot.cwsresult.back.model.mst.point.dao.IPointDao;
import com.appspot.cwsresult.back.model.mst.prize.Prize;
import com.appspot.cwsresult.back.model.mst.prize.dao.IPrizeDao;
import com.appspot.cwsresult.back.zutil.DaoInjector;

public class PrizeManager {

	private static Map<Integer, Prize> prizeMap;
	private static Map<Integer, Point> pointMap;

	private static IPrizeDao prizeDao;
	private static IPointDao pointDao;

	/// Prize //////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<Integer, Prize> getPrizeMap() {
		return prizeMap;
	}

	public static void loadAllPrize(ServletContext servletContext) {
		System.out.println("------- Load all prize start --------");
		prizeMap = getPrizeDao().findAllPrize(servletContext);
		System.out.println("------- Load all prize finish -------");
		System.out.println("");
	}

	private static IPrizeDao getPrizeDao() {
		if (prizeDao == null) {
			prizeDao = (IPrizeDao)DaoInjector.getMstDaoImpl(IPrizeDao.class.getName());
		}
		return prizeDao;
	}

	// Point ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<Integer, Point> getPointMap() {
		return pointMap;
	}

	public static void loadAllPoint(ServletContext servletContext) {
		System.out.println("------- Load all point start --------");
		pointMap = getPointDao().findAllPoint(servletContext);
		System.out.println("------- Load all point finish -------");
		System.out.println("");
	}

	private static IPointDao getPointDao() {
		if (pointDao == null) {
			pointDao = (IPointDao)DaoInjector.getMstDaoImpl(IPointDao.class.getName());
		}
		return pointDao;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
