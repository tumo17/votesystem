package com.appspot.cwsresult.back.manager;

import java.util.Map;

import javax.servlet.ServletContext;

import com.appspot.cwsresult.back.model.mst.applicant.Applicant;
import com.appspot.cwsresult.back.model.mst.applicant.dao.IApplicantDao;
import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.back.model.mst.user.dao.IUserDao;
import com.appspot.cwsresult.back.zutil.DaoInjector;

public class EmpManager {

	private static Map<String, User> userMap;
	private static Map<String, Applicant> applicantMap;

	private static IUserDao userDao;
	private static IApplicantDao applicantDao;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	private EmpManager() {
	}

	// User ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<String, User> getUserMap() {
		return userMap;
	}

	public static void loadAllUser(ServletContext servletContext) {
		System.out.println("------- Load all user start --------");
		userMap = getUserDao().findAllUser(servletContext);
		System.out.println("------- Load all user finish -------");
		System.out.println("");
	}

	private static IUserDao getUserDao() {
		if (userDao == null) {
			userDao = (IUserDao)DaoInjector.getMstDaoImpl(IUserDao.class.getName());
		}
		return userDao;
	}

	// Applicant ///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<String, Applicant> getApplicantMap() {
		return applicantMap;
	}

	public static void loadAllApplicant(ServletContext servletContext) {
		System.out.println("------- Load all applicant start --------");
		applicantMap = getApplicantDao().findAllApplicant(servletContext);
		System.out.println("------- Load all applicant finish -------");
		System.out.println("");
	}

	private static IApplicantDao getApplicantDao() {
		if (applicantDao == null) {
			applicantDao = (IApplicantDao)DaoInjector.getMstDaoImpl(IApplicantDao.class.getName());
		}
		return applicantDao;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
