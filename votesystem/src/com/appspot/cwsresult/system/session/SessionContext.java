package com.appspot.cwsresult.system.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.appspot.cwsresult.back.model.mst.user.User;
import com.appspot.cwsresult.front.controller.certificate.LoginController;
import com.appspot.cwsresult.front.controller.certificate.LogoutController;
import com.appspot.cwsresult.front.controller.main.MainController;

public class SessionContext {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;

	private String controllerName;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public SessionContext(HttpServletRequest req, HttpServletResponse res) {
		this.request = req;
		this.response = res;
		this.session = req.getSession();
	}

	// Request & Response //////////////////////////////////////////////////////////////////////////////////////////////
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setAttribute(String key, Object val) {
		request.setAttribute(key, val);
	}

	public Object getParam(String key) {
		return request.getParameter(key);
	}

	public String getParamString(String key) {
		return (String)request.getParameter(key);
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	// Session /////////////////////////////////////////////////////////////////////////////////////////////////////////
	public HttpSession getSession() {
		return session;
	}

	public User getUser() {
		return (User)session.getAttribute(LOGIN_USER);
	}

	public void certificated() {
		if (LoginController.CONTROLLER_NAME.equals(controllerName)) {
			return;
		}

		if (getUser() == null) {
			controllerName = LogoutController.CONTROLLER_NAME;
		}
	}

	// ControllerName //////////////////////////////////////////////////////////////////////////////////////////////////
	public String getControllerName() {
		return controllerName;
	}

	public void parseRequestUri() {
		String uri = request.getRequestURI();
		int beginIndex = uri.lastIndexOf("/");
		int endIndex = uri.lastIndexOf(".html");

		if (endIndex != -1) {
			controllerName = uri.substring(beginIndex + 1, endIndex);
		} else {
			controllerName = MainController.CONTROLLER_NAME;
		}
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static final String LOGIN_USER = "login_user";
	public static final String AT_COMMAND = "@COMMAND";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
