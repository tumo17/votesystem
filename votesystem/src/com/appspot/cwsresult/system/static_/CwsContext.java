package com.appspot.cwsresult.system.static_;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

public class CwsContext {

	private static ServletContext servletContext;
	private static Map<String, String> serviceMst = Collections.synchronizedMap(new HashMap<String, String>());
	private static Map<String, String> cwsControl = Collections.synchronizedMap(new HashMap<String, String>());
	private static boolean isVoteSystemOpen = false;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	private CwsContext() {
	}

	// ServletContext //////////////////////////////////////////////////////////////////////////////////////////////////
	public static ServletContext getServletContext() {
		return servletContext;
	}

	public static void setServletContext(ServletContext servletContext) {
		CwsContext.servletContext = servletContext;
	}

	// ServiceMst //////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<String, String> getServiceMst() {
		return serviceMst;
	}

	public static void loadServiceMst() {
		System.out.println("------- Load ServiceMst start --------");
		loadProperties(serviceMst, PATH_SERVICE_MST);
		System.out.println("------- Load ServiceMst finish -------");
		System.out.println("");
	}

	// CwsControl //////////////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<String, String> getCwsControl() {
		return cwsControl;
	}

	public static void loadCwsControl() {
		System.out.println("------- Load CwsControl start --------");
		loadProperties(cwsControl, PATH_CWS_CONTROL);
		System.out.println("------- Load CwsControl finish -------");
		System.out.println("");
	}

	// IsOpenVoteSystem ////////////////////////////////////////////////////////////////////////////////////////////////
	public static boolean isVoteSystemOpen() {
		return isVoteSystemOpen;
	}

	public static void setVoteSystemOpen(boolean inputSystemOpen) {
		CwsContext.isVoteSystemOpen = inputSystemOpen;
	}

	// Util ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final void loadProperties(Map<String, String> mstMap, String targetUrl) {
		URL url = null;
		Properties prop = new Properties();
		try {
			url = getServletContext().getResource(targetUrl);
			prop.load(new FileInputStream(url.getPath().replaceAll("%20", " ")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Enumeration<?> en = prop.propertyNames();
		while (en.hasMoreElements()) {
			String key = (String)en.nextElement();
			String val = prop.getProperty(key, "unknown");
			mstMap.put(key, val);
			System.out.println(key + " : " + val);
		}
	}

	// Const ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String PATH_SERVICE_MST = "/WEB-INF/props/service-mst.properties";
	private static final String PATH_CWS_CONTROL = "/WEB-INF/props/cws-control.properties";

	public static final String ADMIN_PASSWORD = "admin_password";
	public static final String LOGIN_PASSWORD = "login_password";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
